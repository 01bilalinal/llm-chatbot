from dotenv import load_dotenv
from flask import Flask, render_template, request
from flask_gtts import gtts
from langchain.callbacks import get_openai_callback
from langchain.chains.question_answering import load_qa_chain
from langchain.document_loaders import UnstructuredURLLoader
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.llms import OpenAI
from langchain.text_splitter import CharacterTextSplitter
from langchain.vectorstores import FAISS

app = Flask(__name__, static_url_path="/static")
load_dotenv()
gtts(app, tempdir='flask_gtts')


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        urls = [
            "https://www.enuygun.com/hakkinda/"
        ]

        loader = UnstructuredURLLoader(urls=urls)

        data = loader.load()
        print(data)
        text_splitter = CharacterTextSplitter(
            separator="\n",
            chunk_size=1000,
            chunk_overlap=200,
            length_function=len
        )
        chunks = text_splitter.split_documents(data)

        embeddings = OpenAIEmbeddings()
        knowledge_base = FAISS.from_documents(chunks, embeddings)

        user_question = request.form['question']

        if user_question:
            llm = OpenAI()
            chain = load_qa_chain(llm, chain_type="stuff")
            with get_openai_callback() as cb:
                docs = knowledge_base.similarity_search(user_question)
                response = chain.run(input_documents=docs, question=user_question)

            return render_template('result.html', response=response)

    return render_template('index.html')


if __name__ == '__main__':
    app.run(debug=True)
